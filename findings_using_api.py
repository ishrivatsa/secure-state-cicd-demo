import json
import requests
import os
import logging

access_token = ''

refresh_token = os.environ['REFRESH_TOKEN']

## Get Access Token from CSP
def auth():
    
    headers = {
        'Content-Type': 'application/x-www-form-urlencoded'
    }
    url = 'https://console.cloud.vmware.com/csp/gateway/am/api/auth/api-tokens/authorize'
    payload = {"refresh_token": refresh_token }

    response = requests.post(url, data=payload , headers=headers)
    
    if response.status_code == 200:
            print("Success")
            logging.info("Successfully received access token") 
    else:
	 	    logging.error("Failed Retrieving auth token" + str(response.status_code))
    
    data = json.loads(response.content)


    global access_token 
    access_token = data["access_token"]
#    print (access_token)

## Get all the findings within the account 
def all_findings():

    global access_token

    headers = {
        'Content-Type' : 'application/json', 
        'Authorization': 'Bearer {}'.format(access_token)
    }
    payload = "{\n}"
    url = 'https://api.securestate.vmware.com/v2/findings/query'
    response = requests.post(url , data=payload, headers=headers)

    #print (response.content)

    # Fetch the continuation Token
    data = json.loads(response.content)
    continuationToken = data["continuationToken"]

    # Get the entire payload for 1000 objects
    headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer {}'.format(access_token)
    }

# replace cloud provider key with "AWS" for Amazon web services related violations
    payload = "{\n\t\"filters\": {\n\t\t\"cloudProvider\": \"AWS\"\n\t}\n, \n\t\"paginationInfo\":{\n\t\t\"continuationToken\": \"" +  continuationToken + "\",\n\t\t\"pageSize\":1000\n\t}\n}"

    url = 'https://api.securestate.vmware.com/v2/findings/query'
    allFindings = requests.post(url , data=payload, headers=headers)

    print (allFindings)

    return allFindings

## Read Terraform output file
def get_terraform_file():

    f = open('deploy/Terraform_Output.json', 'r')
    with open('deploy/Terraform_Output.json') as f:
        read_data = f.read()
    f.close()

    terraformOutput=json.loads(read_data)

    return terraformOutput

## Get all violations related to an ObjectID
def get_violation_by_object(all_findings, objectID):
    violations = []
    violating_resources = []
    data = json.loads(all_findings.content)
    
    ##print (objectID)
    
    ##print (data)
    ## replace ruleId = "5c8c26847a550e1fb6560cab" for Azure and ruleId = "5c8c26417a550e1fb6560c3f" for AWS for port 22 open
    
    for violation in data["results"]:
        if (violation["objectId"] == objectID and violation["status"] == "Open" and violation["ruleId"] == "5c8c25ec7a550e1fb6560bbe"):
            violations.append(violation)
            violating_resources.append(violation["objectId"])

    if(len(violations) > 0):
        return True, violating_resources
    else:
        return False, violating_resources


# Sets Environment variable within gitlab
def SetGitLabVar(val):

    print ("\nViolation Found \n" + str(val))

    url = "https://gitlab.com/api/v4/projects/21330874/variables/VSS_VIOLATION_FOUND"

    payload = "value="+ str(val)
    headers = {
        'private-token': os.environ["SHRI_PRIVATE_TOKEN"],
        'Content-Type': "application/x-www-form-urlencoded",
        }

    print('Setting GitLab Variable')

    response = requests.request("PUT", url, data=payload, headers=headers)

    print("GitLab Status Code " + str(response.status_code))

    if response.status_code == 200:
            print("Success")
            logging.info("Successfully updated variable") 
            os.environ["VSS_VIOLATION_FOUND"] = True
    else:
	 	    logging.error("Could not update Gitlab variable")

	 	 
## Main Function
if __name__ == '__main__':
    ## Auth
    auth()
    ## Get all Findings
    resp = all_findings()

    violation_found = []
    terraformOutput=get_terraform_file()
    objectId = terraformOutput['sg_id']
    
    print("Checking if violations exist \n")
    has_violation,findings_resource_id = get_violation_by_object(resp, objectId['value'])
    
    violation_found.append(has_violation)
    
    print(violation_found)
    print(findings_resource_id)
    count = 0 
    for violation in violation_found:
        if (violation == True):
            count +=1
            SetGitLabVar(True)
            break
        else:
            continue
    if (count == 0):
	    print("No Violations Found !!")